# wmbyte

#### 系统介绍
程序员学习进步，提高工作效率必备工具。
在线一键生成CRUD代码，代码美化工具

#### 软件架构
软件架构说明

**前端:Vue 3 + TypeScript + Vite + AntDesign**

**NodeJS版本：v18.14.2**

#### 安装教程

1. npm install
2. npm run dev

#### 使用说明

1. 代码生成器
> 界面化配置表结构，一键生成对应表结构语句以及Java相关代码

![mmqrcode1610862757073.png](https://pic.imgdb.cn/item/6422c7aca682492fccbd3206.png)
2. JSON美化
> 粘贴您的JSON数据，快速进行格式化。
![mmqrcode1610862757073.png](https://pic.imgdb.cn/item/6422c82ba682492fccbe27c9.png)
3. 代码美化
> 支持HTML、JS、CSS、SQL代码美化，看代码眼睛不在受累
![mmqrcode1610862757073.png](https://pic.imgdb.cn/item/6422c839a682492fccbe4477.png)
4. 技术文章
> 查看站长分享的技术文章
5. 源码空间
> 好东西，一起分享，源码分享。

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 作者介绍
**程序员无名**

### 感谢支持与关注
### 欢迎一起参与讨论与交流
### 加我微信，进交流群
![mmqrcode1610862757073.png](https://i.loli.net/2021/01/17/gonPYuDRzUxlAJT.jpg)
### 关注微信公众号"程序员无名",了解更多技术知识
![_export1610862720016.jpg.png](https://wmbyte.oss-cn-shanghai.aliyuncs.com/%E5%85%AC%E4%BC%97%E5%8F%B7%E4%BA%8C%E7%BB%B4%E7%A0%81.jpg)




