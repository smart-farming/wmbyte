import * as monaco from "monaco-editor";

export type FormState ={
    tableName: string;
    tableComment: string;
    tablePrefix:string;
}

export type Item = {
    id: string;
    fieldName: string;
    fieldType: string;
    fieldLen: string;
    fieldDefult:string;
    fieldComment:string;
    notEmpty:boolean;
    isPrimaryKey:boolean;
    isAutoIncrement:boolean
    editable?: boolean;
};

export type editorType = {
    key:string,
    value:monaco.editor.IStandaloneCodeEditor
}
