import {TreeProps} from "ant-design-vue";

export interface GetArticleList { // 文章数据
    (data: GetArticleListRequest): Promise<ArticleDataListResponse>
}

export interface GetArticleListRequest { // 入参
    id: string
}

export type ArticleDataListResponse = MResponse<ArticleData> // 文章返回值类型
export type ArticleTypeDataListResponse = MResponse<TreeProps['treeData']> // 文章类型返回值类型

//文章数据类型
export interface ArticleData {
    id:string,
    title:string,
    content:string,
    pid:string
}

