interface MResponse<T> {
    code: string,
    msg: string,
    data?: T
};
