import { Router, createRouter, createWebHashHistory } from "vue-router";
const CreateRouter: () => Router = () => createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: "/",
            name: "index",
            component: () => import("@/views/index.vue"),
            redirect:"/codegenerator",
            children:[
                {
                    path:"/codegenerator",
                    name:"codegenerator",
                    component: () => import("@/views/codegenerator.vue"),
                },
                {
                    path:"/jsonformat",
                    name:"jsonformat",
                    component: () => import("@/views/jsonformat.vue"),
                },
                {
                    path:"/codeformat",
                    name:"codeformat",
                    component: () => import("@/views/codeformat.vue"),
                },
                {
                    path:"/article",
                    name:"article",
                    component: () => import("@/views/article.vue"),
                }
            ]
        }
    ]
})

const router: Router = CreateRouter();

export default router
