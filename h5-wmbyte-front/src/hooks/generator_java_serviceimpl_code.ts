import {FormState, Item} from '_types/codegenerator'
import dayjs from "dayjs";
import * as strUtils from "_hooks/utils/StrUtils"
import {mySqlTypeToJavaType} from "_hooks/constants/Constants"

//生成java基于Mybatis-Plus的DAO
export const generator_java_serviceimpl_code=(form:FormState,items:Item[],daoType:string)=>{
    let nowDate =  dayjs(Date.now()).format("YYYY-MM-DD HH:mm:ss")
    let importContent=new Set<string>();
    importContent.add("import org.springframework.stereotype.Service;")
    if(daoType === "mp"){
        importContent.add("import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;")
    }
    let fieldContent = "";
    //去除前缀
    let replacePrefix = strUtils.replacePrefix(form.tablePrefix,form.tableName)
    //下划线转驼峰命名法
    let className = strUtils.capitalize(form.tableName);


    const javaServiceImplTemplate=
        `
${Array.from(importContent).join("\n")}
/**
 * @author 
 * @description ${form.tableComment}ServiceImpl类
 * @createDate ${nowDate}
 **/
@Service
public interface ${className}ServiceImpl extends ServiceImpl<${className}Dao,${className}> implements ${className}Service {
}`;
    return javaServiceImplTemplate;
}
