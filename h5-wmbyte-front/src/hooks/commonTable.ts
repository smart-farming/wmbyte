//可以选择的常用表
import {FormState, Item} from "_types/codegenerator";
import {import_mysql_default_column} from "_hooks/import_mysql_default_column"
import { Ref} from 'vue';

export const commonTableOptions:Map<string,string> = new Map([
    ["t_products","商品表"],
    ["t_orders","订单表"],
    // ["t_customers","用户表"],
    // ["t_articles","文章表"],
    // ["t_comments","评论表"],
    // ["t_employees","员工表"],
])

export const commonTableColumns:Map<string,Item[]> = new Map([
    ["t_products",[
        {
            id: "common_table_key_product_id",
            fieldName: "product_id",
            fieldType: "bigint",
            fieldLen: "",
            fieldDefult:"",
            fieldComment:"主键",
            notEmpty:true,
            isPrimaryKey:true,
            isAutoIncrement:true,
            editable: true
        },{
            id: "common_table_key_product_name",
            fieldName: "product_name",
            fieldType: "VARCHAR",
            fieldLen: "50",
            fieldDefult:"",
            fieldComment:"商品名称",
            notEmpty:true,
            isPrimaryKey:false,
            isAutoIncrement:false,
            editable: true
        },{
            id: "common_table_key_product_desc",
            fieldName: "product_desc",
            fieldType: "TEXT",
            fieldLen: "",
            fieldDefult:"",
            fieldComment:"商品描述",
            notEmpty:false,
            isPrimaryKey:false,
            isAutoIncrement:false,
            editable: true
        },{
            id: "common_table_key_product_price",
            fieldName: "product_price",
            fieldType: "DECIMAL",
            fieldLen: "10,2",
            fieldDefult:"",
            fieldComment:"商品价格",
            notEmpty:true,
            isPrimaryKey:false,
            isAutoIncrement:false,
            editable: true
        },{
            id: "common_table_key_product_quantity",
            fieldName: "product_quantity",
            fieldType: "INT",
            fieldLen: "",
            fieldDefult:"",
            fieldComment:"商品数量",
            notEmpty:true,
            isPrimaryKey:false,
            isAutoIncrement:false,
            editable: true
        }
    ]],
    ["t_orders",[
        {
            id: "common_table_key_order_id",
            fieldName: "order_id",
            fieldType: "bigint",
            fieldLen: "",
            fieldDefult:"",
            fieldComment:"主键",
            notEmpty:true,
            isPrimaryKey:true,
            isAutoIncrement:true,
            editable: true
        },{
            id: "common_table_key_order_date",
            fieldName: "order_date",
            fieldType: "DATE",
            fieldLen: "",
            fieldDefult:"",
            fieldComment:"下单日期",
            notEmpty:true,
            isPrimaryKey:false,
            isAutoIncrement:false,
            editable: true
        },{
            id: "common_table_key_order_total_amount",
            fieldName: "order_total_amount",
            fieldType: "DECIMAL",
            fieldLen: "10,2",
            fieldDefult:"",
            fieldComment:"订单总金额",
            notEmpty:true,
            isPrimaryKey:false,
            isAutoIncrement:false,
            editable: true
        },{
            id: "common_table_key_order_status",
            fieldName: "order_status",
            fieldType: "INT",
            fieldLen: "",
            fieldDefult:"",
            fieldComment:"订单状态",
            notEmpty:true,
            isPrimaryKey:false,
            isAutoIncrement:false,
            editable: true
        },{
            id: "common_table_key_customer_id",
            fieldName: "customer_id",
            fieldType: "BIGINT",
            fieldLen: "",
            fieldDefult:"",
            fieldComment:"用户ID",
            notEmpty:true,
            isPrimaryKey:false,
            isAutoIncrement:false,
            editable: true
        },{
            id: "common_table_key_pay_method",
            fieldName: "pay_method",
            fieldType: "INT",
            fieldLen: "",
            fieldDefult:"",
            fieldComment:"支付方式",
            notEmpty:true,
            isPrimaryKey:false,
            isAutoIncrement:false,
            editable: true
        },{
            id: "common_table_key_shipping_address",
            fieldName: "shipping_address",
            fieldType: "VARCHAR",
            fieldLen: "300",
            fieldDefult:"",
            fieldComment:"收货地址",
            notEmpty:true,
            isPrimaryKey:false,
            isAutoIncrement:false,
            editable: true
        },{
            id: "common_table_key_billing_address",
            fieldName: "billing_address",
            fieldType: "VARCHAR",
            fieldLen: "300",
            fieldDefult:"",
            fieldComment:"发货地址",
            notEmpty:true,
            isPrimaryKey:false,
            isAutoIncrement:false,
            editable: true
        }
    ]]
])

//选择某一个常用表以后的事件触发函数
export const commonTableChange=(
    datasource:Ref<Item[]>,
    FormState:Ref<FormState>,
    key:string
    )=>{
    let items:any=commonTableColumns.get(key)
    FormState.value.tableName = key;
    FormState.value.tableComment = commonTableOptions.get(key)||""
    FormState.value.tablePrefix = "t_"
    let array= import_mysql_default_column();
    //将items增加到array第一个元素之后
    array.splice(1, 0, ...items);
    datasource.value = array
}




