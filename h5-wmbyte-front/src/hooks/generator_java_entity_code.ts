import {FormState, Item} from '_types/codegenerator'
import dayjs from "dayjs";
import * as strUtils from "_hooks/utils/StrUtils"
import {mySqlTypeToJavaType} from "_hooks/constants/Constants"

//根据框架不同，引入不通的包
const importContentFun = (daoType:string)=>{
    let importContent=new Set<string>();
    importContent.add("import lombok.Data;")
    switch (daoType){
        case "mp":
            importContent.add("import com.baomidou.mybatisplus.annotation.TableName;")
            break;
        case "m":
            break;
    }
    return importContent;
}


//生成java实体类
export const generator_java_entity_code=(form:FormState,items:Item[],daoType:string)=>{
    let nowDate =  dayjs(Date.now()).format("YYYY-MM-DD HH:mm:ss")
    let importContent=importContentFun(daoType);
    let fieldContent = "";
    //mybatis-plus，添加注解标识表名
    let classAnnotation = (daoType=="mp")?`\n@TableName("${form.tableName}")`:"";
    items.forEach((item)=>{
      //数据类型
      let fieldType = mySqlTypeToJavaType.get(item.fieldType)||"";
      if(fieldType.indexOf(".")>0){
          //需要引入额外的包
          importContent.add(`import ${fieldType};`);
          //从引入的包中截取具体需要的类型，例如java.util.Date =====> Date
          fieldType = fieldType.substring(fieldType.lastIndexOf(".")+1,fieldType.length);
      }
      //属性名驼峰命名
      let fieldName = strUtils.underlineToCamel(item.fieldName);
      //注释
      fieldContent+=`\t//${item.fieldComment}\n`;
      //具体属性
      fieldContent+=`\tprivate ${fieldType} ${fieldName};\n`;
    })
    //去除前缀
    let className = strUtils.replacePrefix(form.tablePrefix,form.tableName)
    //下划线转驼峰命名法
    className = strUtils.capitalize(className);
const javaEntityTemplate=
`
${Array.from(importContent).join("\n")}
/**
 * @author 
 * @description ${form.tableComment}实体类
 * @createDate ${nowDate}
 **/
@Data${classAnnotation}
public class ${className} {

${fieldContent}

}`;
    return javaEntityTemplate;
}
