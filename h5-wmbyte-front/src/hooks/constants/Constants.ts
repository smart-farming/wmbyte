export const mySqlTypeToJavaType: Map<string, string> = new Map([
    ["TINYINT", "Integer"],
    ["SMALLINT", "Integer"],
    ["MEDIUMINT", "Integer"],
    ["INT", "Integer"],
    ["BIGINT", "Long"],
    ["FLOAT", "Float"],
    ["DOUBLE", "Double"],
    ["DECIMAL", "java.math.BigDecimal"],
    ["DATE", "java.util.Date"],
    ["DATETIME", "java.util.Date"],
    ["TIMESTAMP", "java.util.Date"],
    ["CHAR", "String"],
    ["VARCHAR", "String"],
    ["TINYBLOB", "byte[]"],
    ["BLOB", "byte[]"],
    ["MEDIUMBLOB", "byte[]"],
    ["LONGBLOB", "byte[]"],
    ["TINYTEXT", "String"],
    ["TEXT", "String"],
    ["MEDIUMTEXT", "String"],
    ["LONGTEXT", "String"],
    ["ENUM", "String"],
    ["SET", "String"],
    ["JSON", "com.alibaba.fastjson.JSONObject"],
]);

export const daoType:Map<string,string> = new Map([
    ["mp","Mybatis-Plus"],
    ["m","Mybatis"]
])
