import {Item} from "_types/codegenerator";

//导入标准字段
export const import_mysql_default_column=()=>{
    const items:Item[] = [{
        id: "key_id",
        fieldName: "id",
        fieldType: "BIGINT",
        fieldLen: "20",
        fieldDefult:"",
        fieldComment:"主键ID",
        notEmpty:true,
        isPrimaryKey:true,
        isAutoIncrement:true,
        editable: true
    },{
        id: "key_create_time",
        fieldName: "create_time",
        fieldType: "DATETIME",
        fieldLen: "",
        fieldDefult:"",
        fieldComment:"创建时间",
        notEmpty:false,
        isPrimaryKey:false,
        isAutoIncrement:false,
        editable: true
    },{
        id: "key_update_time",
        fieldName: "update_time",
        fieldType: "DATETIME",
        fieldLen: "",
        fieldDefult:"",
        fieldComment:"修改时间",
        notEmpty:false,
        isPrimaryKey:false,
        isAutoIncrement:false,
        editable: true
    },{
        id: "key_create_by",
        fieldName: "create_by",
        fieldType: "BIGINT",
        fieldLen: "",
        fieldDefult:"",
        fieldComment:"创建人",
        notEmpty:false,
        isPrimaryKey:false,
        isAutoIncrement:false,
        editable: true
    },{
        id: "key_update_by",
        fieldName: "update_by",
        fieldType: "BIGINT",
        fieldLen: "",
        fieldDefult:"",
        fieldComment:"修改人",
        notEmpty:false,
        isPrimaryKey:false,
        isAutoIncrement:false,
        editable: true
    },{
        id: "key_status",
        fieldName: "status",
        fieldType: "INT",
        fieldLen: "",
        fieldDefult:"",
        fieldComment:"状态",
        notEmpty:false,
        isPrimaryKey:false,
        isAutoIncrement:false,
        editable: true
    }]
    return items;
}
