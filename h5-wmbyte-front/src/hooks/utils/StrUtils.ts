

//首字母变大写
export const capitalize=(str:string)=>{
    return str.charAt(0).toUpperCase() + str.slice(1);
}

//下划线转驼峰
export const underlineToCamel=(str:string)=>{
    return str.replace(/_([a-z])/g, (match, p1) => p1.toUpperCase());
}

//去除前缀
export const replacePrefix=(prefix:string,str:string)=>{
    if (str.startsWith(prefix)) {
        str = str.substring(prefix.length);
    }
    return str;
}

