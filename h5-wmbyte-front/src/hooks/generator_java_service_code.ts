import {FormState, Item} from '_types/codegenerator'
import dayjs from "dayjs";
import * as strUtils from "_hooks/utils/StrUtils"
import {mySqlTypeToJavaType} from "_hooks/constants/Constants"

//生成java基于Mybatis-Plus的Service
export const generator_java_service_code=(form:FormState,items:Item[],daoType:string)=>{
    let nowDate =  dayjs(Date.now()).format("YYYY-MM-DD HH:mm:ss")
    let importContent=new Set<string>();
    if(daoType === 'mp'){
        importContent.add("import com.baomidou.mybatisplus.extension.service.IService;")
    }
    //去除前缀
    let replacePrefix = strUtils.replacePrefix(form.tablePrefix,form.tableName)
    //下划线转驼峰命名法
    let className = strUtils.capitalize(replacePrefix);


    const javaServiceTemplate=
        `
${Array.from(importContent).join("\n")}
/**
 * @author 
 * @description ${form.tableComment}Service类
 * @createDate ${nowDate}
 **/
@Mapper
public interface ${className}Service extends IService<${className}> {
}`;
    return javaServiceTemplate;
}
