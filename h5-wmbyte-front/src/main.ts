import { createApp } from 'vue'
import App from './App'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.css'
import router from './router'
import { createPinia } from "pinia"
import VMdPreview from '@kangc/v-md-editor/lib/preview';
import '@kangc/v-md-editor/lib/style/preview.css';
import githubTheme from '@kangc/v-md-editor/lib/theme/github.js';
import '@kangc/v-md-editor/lib/theme/style/github.css';
// highlightjs
import hljs from 'highlight.js';
import createCopyCodePlugin from '@kangc/v-md-editor/lib/plugins/copy-code/index';
import '@kangc/v-md-editor/lib/plugins/copy-code/copy-code.css';
VMdPreview.use(githubTheme, {
    Hljs: hljs
});
VMdPreview.use(createCopyCodePlugin())
const app =createApp(App)

app.use(createPinia())
app.use(VMdPreview)
app.use(Antd);


app.use(router).mount('#app')

export  default app
