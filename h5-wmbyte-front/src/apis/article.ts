import {
    ArticleDataListResponse,
    ArticleTypeDataListResponse,
    GetArticleList,
    GetArticleListRequest,
} from "_types/article";
import Request from "@/utils/axios";
export const getArticleList: GetArticleList = (data) => {
    return Request<GetArticleListRequest, ArticleDataListResponse>({
        url: `article/getArticle?id=${data.id}`,
        method: "get"
    })
}

export const getArticleTypeList = () => {
    return Request<null, ArticleTypeDataListResponse>({
        url: `article/getTypeList`,
        method: "get"
    })
}
