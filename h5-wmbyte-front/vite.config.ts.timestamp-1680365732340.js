// vite.config.ts
import { defineConfig, loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";
import Components from "unplugin-vue-components/vite";
import AutoImport from "unplugin-auto-import/vite";
import { AntDesignVueResolver } from "unplugin-vue-components/resolvers";
import { prismjs } from "prismjs";
import { resolve } from "path";
var vite_config_default = ({ mode }) => {
  return defineConfig({
    base: "./",
    server: {
      port: 9002,
      proxy: {
        "^/wmbyte/": {
          target: loadEnv(mode, process.cwd()).VITE_AXIOS_PROXY_URL,
          changeOrigin: true,
          secure: false,
          rewrite: (path) => path.replace(/^\/wmbyte/, "/wmbyte/")
        }
      },
      host: "0.0.0.0"
    },
    plugins: [
      vue(),
      prismjs({
        languages: ["all"],
        plugins: ["line-numbers"],
        theme: "coy",
        css: true
      }),
      vueJsx({
        transformOn: true
      }),
      AutoImport({
        imports: [
          "vue",
          "vue-router"
        ],
        dts: "src/auto-imports.d.ts"
      }),
      Components({
        dts: "src/components.d.ts",
        deep: true,
        dirs: ["src/components"],
        extensions: ["vue", "tsx"],
        resolvers: [AntDesignVueResolver()]
      })
    ],
    css: {
      preprocessorOptions: {
        less: {
          modifyVars: {
            "color-primary-6": "rgb(238,90,70)",
            "color-primary-5": "rgb(246,125,114)",
            "color-primary-7": "rgb(246,125,114)",
            "btn-primary-color-bg_disabled": "#efcfcb"
          },
          additionalData: `@import "${resolve("/Users/lisw/work/my-project/wmbyte/h5-wmbyte-front", "src/assets/styles/base.less")}";`,
          javascriptEnabled: true
        }
      }
    },
    resolve: {
      alias: {
        "@": resolve("/Users/lisw/work/my-project/wmbyte/h5-wmbyte-front", ".", "src/"),
        "_styles": resolve("/Users/lisw/work/my-project/wmbyte/h5-wmbyte-front", ".", "src/assets/styles/"),
        "_apis": resolve("/Users/lisw/work/my-project/wmbyte/h5-wmbyte-front", ".", "src/apis/"),
        "_utils": resolve("/Users/lisw/work/my-project/wmbyte/h5-wmbyte-front", ".", "src/utils/"),
        "_types": resolve("/Users/lisw/work/my-project/wmbyte/h5-wmbyte-front", ".", "src/types/"),
        "_hooks": resolve("/Users/lisw/work/my-project/wmbyte/h5-wmbyte-front", ".", "src/hooks/"),
        "_stores": resolve("/Users/lisw/work/my-project/wmbyte/h5-wmbyte-front", ".", "src/stores/"),
        "_workers": resolve("/Users/lisw/work/my-project/wmbyte/h5-wmbyte-front", ".", "src/workers/"),
        "_components": resolve("/Users/lisw/work/my-project/wmbyte/h5-wmbyte-front", ".", "src/components/")
      }
    }
  });
};
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcudHMiXSwKICAic291cmNlc0NvbnRlbnQiOiBbImltcG9ydCB7IGRlZmluZUNvbmZpZywgbG9hZEVudiB9IGZyb20gJ3ZpdGUnO1xuaW1wb3J0IHZ1ZSBmcm9tICdAdml0ZWpzL3BsdWdpbi12dWUnO1xuaW1wb3J0IHZ1ZUpzeCBmcm9tICdAdml0ZWpzL3BsdWdpbi12dWUtanN4JztcbmltcG9ydCBDb21wb25lbnRzIGZyb20gJ3VucGx1Z2luLXZ1ZS1jb21wb25lbnRzL3ZpdGUnO1xuaW1wb3J0IEF1dG9JbXBvcnQgZnJvbSAndW5wbHVnaW4tYXV0by1pbXBvcnQvdml0ZSdcbmltcG9ydCB7IEFudERlc2lnblZ1ZVJlc29sdmVyIH0gZnJvbSBcInVucGx1Z2luLXZ1ZS1jb21wb25lbnRzL3Jlc29sdmVyc1wiO1xuaW1wb3J0IHsgcHJpc21qcyB9IGZyb20gJ3ByaXNtanMnXG5pbXBvcnQgeyByZXNvbHZlIH0gZnJvbSBcInBhdGhcIlxuLy8gaHR0cHM6Ly92aXRlanMuZGV2L2NvbmZpZy9cbmV4cG9ydCBkZWZhdWx0ICh7IG1vZGUgfSkgPT4ge1xuICByZXR1cm4gZGVmaW5lQ29uZmlnKHtcbiAgICBiYXNlOiBcIi4vXCIsXG4gICAgc2VydmVyOiB7XG4gICAgICBwb3J0OiA5MDAyLFxuICAgICAgcHJveHk6IHtcbiAgICAgICAgXCJeL3dtYnl0ZS9cIjoge1xuICAgICAgICAgIHRhcmdldDogbG9hZEVudihtb2RlLCBwcm9jZXNzLmN3ZCgpKS5WSVRFX0FYSU9TX1BST1hZX1VSTCxcbiAgICAgICAgICBjaGFuZ2VPcmlnaW46IHRydWUsXG4gICAgICAgICAgc2VjdXJlOiBmYWxzZSxcbiAgICAgICAgICByZXdyaXRlOiAocGF0aCkgPT4gcGF0aC5yZXBsYWNlKC9eXFwvd21ieXRlLywgJy93bWJ5dGUvJyksXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICBob3N0OiAnMC4wLjAuMCdcbiAgICB9LFxuICAgIHBsdWdpbnM6IFtcbiAgICAgIHZ1ZSgpLFxuICAgICAgcHJpc21qcyh7XG4gICAgICAgIGxhbmd1YWdlczogWydhbGwnXSxcbiAgICAgICAgLy8gXHU5MTREXHU3RjZFXHU4ODRDXHU1M0Y3XHU2M0QyXHU0RUY2XG4gICAgICAgIHBsdWdpbnM6IFsnbGluZS1udW1iZXJzJ10sXG4gICAgICAgIC8vIFx1NEUzQlx1OTg5OFx1NTQwRFxuICAgICAgICB0aGVtZTogJ2NveScsXG4gICAgICAgIGNzczogdHJ1ZVxuICAgICAgfSksXG4gICAgICB2dWVKc3goe1xuICAgICAgICB0cmFuc2Zvcm1PbjogdHJ1ZVxuICAgICAgfSksXG4gICAgICBBdXRvSW1wb3J0KHtcbiAgICAgICAgaW1wb3J0czogW1xuICAgICAgICAgICd2dWUnLFxuICAgICAgICAgICd2dWUtcm91dGVyJyxcbiAgICAgICAgXSxcbiAgICAgICAgZHRzOiBcInNyYy9hdXRvLWltcG9ydHMuZC50c1wiXG4gICAgICB9KSxcbiAgICAgIENvbXBvbmVudHMoe1xuICAgICAgICBkdHM6IFwic3JjL2NvbXBvbmVudHMuZC50c1wiLFxuICAgICAgICBkZWVwOiB0cnVlLFxuICAgICAgICBkaXJzOiBbXCJzcmMvY29tcG9uZW50c1wiXSxcbiAgICAgICAgZXh0ZW5zaW9uczogW1widnVlXCIsIFwidHN4XCJdLFxuICAgICAgICByZXNvbHZlcnM6IFtBbnREZXNpZ25WdWVSZXNvbHZlcigpXSxcbiAgICAgIH0pLFxuICAgIF0sXG4gICAgY3NzOiB7XG4gICAgICBwcmVwcm9jZXNzb3JPcHRpb25zOiB7XG4gICAgICAgIGxlc3M6IHtcbiAgICAgICAgICBtb2RpZnlWYXJzOiB7XG4gICAgICAgICAgICAnY29sb3ItcHJpbWFyeS02JzogXCJyZ2IoMjM4LDkwLDcwKVwiLFxuICAgICAgICAgICAgJ2NvbG9yLXByaW1hcnktNSc6IFwicmdiKDI0NiwxMjUsMTE0KVwiLFxuICAgICAgICAgICAgJ2NvbG9yLXByaW1hcnktNyc6IFwicmdiKDI0NiwxMjUsMTE0KVwiLFxuICAgICAgICAgICAgJ2J0bi1wcmltYXJ5LWNvbG9yLWJnX2Rpc2FibGVkJzonI2VmY2ZjYicsXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhZGRpdGlvbmFsRGF0YTogYEBpbXBvcnQgXCIke3Jlc29sdmUoXCIvVXNlcnMvbGlzdy93b3JrL215LXByb2plY3Qvd21ieXRlL2g1LXdtYnl0ZS1mcm9udFwiLCAnc3JjL2Fzc2V0cy9zdHlsZXMvYmFzZS5sZXNzJyl9XCI7YCxcbiAgICAgICAgICBqYXZhc2NyaXB0RW5hYmxlZDogdHJ1ZSxcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH0sXG4gICAgcmVzb2x2ZToge1xuICAgICAgYWxpYXM6IHtcbiAgICAgICAgXCJAXCI6IHJlc29sdmUoXCIvVXNlcnMvbGlzdy93b3JrL215LXByb2plY3Qvd21ieXRlL2g1LXdtYnl0ZS1mcm9udFwiLCAnLicsICdzcmMvJyksXG4gICAgICAgIFwiX3N0eWxlc1wiOiByZXNvbHZlKFwiL1VzZXJzL2xpc3cvd29yay9teS1wcm9qZWN0L3dtYnl0ZS9oNS13bWJ5dGUtZnJvbnRcIiwgJy4nLCAnc3JjL2Fzc2V0cy9zdHlsZXMvJyksXG4gICAgICAgIFwiX2FwaXNcIjogcmVzb2x2ZShcIi9Vc2Vycy9saXN3L3dvcmsvbXktcHJvamVjdC93bWJ5dGUvaDUtd21ieXRlLWZyb250XCIsICcuJywgJ3NyYy9hcGlzLycpLFxuICAgICAgICBcIl91dGlsc1wiOiByZXNvbHZlKFwiL1VzZXJzL2xpc3cvd29yay9teS1wcm9qZWN0L3dtYnl0ZS9oNS13bWJ5dGUtZnJvbnRcIiwgJy4nLCAnc3JjL3V0aWxzLycpLFxuICAgICAgICBcIl90eXBlc1wiOiByZXNvbHZlKFwiL1VzZXJzL2xpc3cvd29yay9teS1wcm9qZWN0L3dtYnl0ZS9oNS13bWJ5dGUtZnJvbnRcIiwgJy4nLCAnc3JjL3R5cGVzLycpLFxuICAgICAgICBcIl9ob29rc1wiOiByZXNvbHZlKFwiL1VzZXJzL2xpc3cvd29yay9teS1wcm9qZWN0L3dtYnl0ZS9oNS13bWJ5dGUtZnJvbnRcIiwgJy4nLCAnc3JjL2hvb2tzLycpLFxuICAgICAgICBcIl9zdG9yZXNcIjogcmVzb2x2ZShcIi9Vc2Vycy9saXN3L3dvcmsvbXktcHJvamVjdC93bWJ5dGUvaDUtd21ieXRlLWZyb250XCIsICcuJywgJ3NyYy9zdG9yZXMvJyksXG4gICAgICAgIFwiX3dvcmtlcnNcIjogcmVzb2x2ZShcIi9Vc2Vycy9saXN3L3dvcmsvbXktcHJvamVjdC93bWJ5dGUvaDUtd21ieXRlLWZyb250XCIsICcuJywgJ3NyYy93b3JrZXJzLycpLFxuICAgICAgICBcIl9jb21wb25lbnRzXCI6IHJlc29sdmUoXCIvVXNlcnMvbGlzdy93b3JrL215LXByb2plY3Qvd21ieXRlL2g1LXdtYnl0ZS1mcm9udFwiLCAnLicsICdzcmMvY29tcG9uZW50cy8nKSxcbiAgICAgIH1cbiAgICB9XG4gIH0pXG59XG4iXSwKICAibWFwcGluZ3MiOiAiO0FBQUEsU0FBUyxjQUFjLGVBQWU7QUFDdEMsT0FBTyxTQUFTO0FBQ2hCLE9BQU8sWUFBWTtBQUNuQixPQUFPLGdCQUFnQjtBQUN2QixPQUFPLGdCQUFnQjtBQUN2QixTQUFTLDRCQUE0QjtBQUNyQyxTQUFTLGVBQWU7QUFDeEIsU0FBUyxlQUFlO0FBRXhCLElBQU8sc0JBQVEsQ0FBQyxFQUFFLEtBQUssTUFBTTtBQUMzQixTQUFPLGFBQWE7QUFBQSxJQUNsQixNQUFNO0FBQUEsSUFDTixRQUFRO0FBQUEsTUFDTixNQUFNO0FBQUEsTUFDTixPQUFPO0FBQUEsUUFDTCxhQUFhO0FBQUEsVUFDWCxRQUFRLFFBQVEsTUFBTSxRQUFRLElBQUksQ0FBQyxFQUFFO0FBQUEsVUFDckMsY0FBYztBQUFBLFVBQ2QsUUFBUTtBQUFBLFVBQ1IsU0FBUyxDQUFDLFNBQVMsS0FBSyxRQUFRLGFBQWEsVUFBVTtBQUFBLFFBQ3pEO0FBQUEsTUFDRjtBQUFBLE1BQ0EsTUFBTTtBQUFBLElBQ1I7QUFBQSxJQUNBLFNBQVM7QUFBQSxNQUNQLElBQUk7QUFBQSxNQUNKLFFBQVE7QUFBQSxRQUNOLFdBQVcsQ0FBQyxLQUFLO0FBQUEsUUFFakIsU0FBUyxDQUFDLGNBQWM7QUFBQSxRQUV4QixPQUFPO0FBQUEsUUFDUCxLQUFLO0FBQUEsTUFDUCxDQUFDO0FBQUEsTUFDRCxPQUFPO0FBQUEsUUFDTCxhQUFhO0FBQUEsTUFDZixDQUFDO0FBQUEsTUFDRCxXQUFXO0FBQUEsUUFDVCxTQUFTO0FBQUEsVUFDUDtBQUFBLFVBQ0E7QUFBQSxRQUNGO0FBQUEsUUFDQSxLQUFLO0FBQUEsTUFDUCxDQUFDO0FBQUEsTUFDRCxXQUFXO0FBQUEsUUFDVCxLQUFLO0FBQUEsUUFDTCxNQUFNO0FBQUEsUUFDTixNQUFNLENBQUMsZ0JBQWdCO0FBQUEsUUFDdkIsWUFBWSxDQUFDLE9BQU8sS0FBSztBQUFBLFFBQ3pCLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQztBQUFBLE1BQ3BDLENBQUM7QUFBQSxJQUNIO0FBQUEsSUFDQSxLQUFLO0FBQUEsTUFDSCxxQkFBcUI7QUFBQSxRQUNuQixNQUFNO0FBQUEsVUFDSixZQUFZO0FBQUEsWUFDVixtQkFBbUI7QUFBQSxZQUNuQixtQkFBbUI7QUFBQSxZQUNuQixtQkFBbUI7QUFBQSxZQUNuQixpQ0FBZ0M7QUFBQSxVQUNsQztBQUFBLFVBQ0EsZ0JBQWdCLFlBQVksUUFBUSxzREFBc0QsNkJBQTZCO0FBQUEsVUFDdkgsbUJBQW1CO0FBQUEsUUFDckI7QUFBQSxNQUNGO0FBQUEsSUFDRjtBQUFBLElBQ0EsU0FBUztBQUFBLE1BQ1AsT0FBTztBQUFBLFFBQ0wsS0FBSyxRQUFRLHNEQUFzRCxLQUFLLE1BQU07QUFBQSxRQUM5RSxXQUFXLFFBQVEsc0RBQXNELEtBQUssb0JBQW9CO0FBQUEsUUFDbEcsU0FBUyxRQUFRLHNEQUFzRCxLQUFLLFdBQVc7QUFBQSxRQUN2RixVQUFVLFFBQVEsc0RBQXNELEtBQUssWUFBWTtBQUFBLFFBQ3pGLFVBQVUsUUFBUSxzREFBc0QsS0FBSyxZQUFZO0FBQUEsUUFDekYsVUFBVSxRQUFRLHNEQUFzRCxLQUFLLFlBQVk7QUFBQSxRQUN6RixXQUFXLFFBQVEsc0RBQXNELEtBQUssYUFBYTtBQUFBLFFBQzNGLFlBQVksUUFBUSxzREFBc0QsS0FBSyxjQUFjO0FBQUEsUUFDN0YsZUFBZSxRQUFRLHNEQUFzRCxLQUFLLGlCQUFpQjtBQUFBLE1BQ3JHO0FBQUEsSUFDRjtBQUFBLEVBQ0YsQ0FBQztBQUNIOyIsCiAgIm5hbWVzIjogW10KfQo=
