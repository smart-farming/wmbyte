package com.wmbyte.modules.article.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wmbyte.modules.article.dto.ArticleTypeTreeDto;
import com.wmbyte.modules.article.entity.Article;
import com.wmbyte.modules.article.entity.ArticleType;
import com.wmbyte.modules.article.mapper.ArticleMapper;
import com.wmbyte.modules.article.mapper.ArticleTypeMapper;
import com.wmbyte.modules.article.service.ArticleTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lisw
 * @program wmbyte
 * @description
 * @createDate 2023-04-02 11:32:54
 **/
@Service
public class ArticleTypeServiceImpl extends ServiceImpl<ArticleTypeMapper, ArticleType> implements ArticleTypeService {

    @Autowired
    private ArticleMapper articleMapper;


    @Override
    public List<ArticleTypeTreeDto> listAll() {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByAsc("sort");
        List<ArticleType> list = this.list(queryWrapper);
        List<Article> articles = articleMapper.selectList(null);
        Map<Long, List<Article>> articlePidGroup = articles.stream().collect(Collectors.groupingBy(Article::getPid));
        List<ArticleTypeTreeDto> articleTypeTreeDtos = new ArrayList<>();
        for (ArticleType articleType : list) {
            if(articleType.getPid() == 0L){
                ArticleTypeTreeDto articleTypeTreeDto = new ArticleTypeTreeDto();
                String key = articleType.getId().toString();
                articleTypeTreeDto.setKey(key);
                articleTypeTreeDto.setTitle(articleType.getName());
                List<ArticleTypeTreeDto> child = null;
                //调用递归方法，获取下级目录，方法已实现，先注释，减少开销。后续目录层级增多时，进行放开
//                List<ArticleTypeTreeDto> child = getChild(articleType.getId(), list, key,articlePidGroup);
                List<ArticleTypeTreeDto> childArticle = getChildArticle(articlePidGroup, key, articleType.getId());
                if(child == null){
                    child = childArticle;
                }else{
                    child.addAll(childArticle);
                }
                articleTypeTreeDto.setChildren(child);
                articleTypeTreeDtos.add(articleTypeTreeDto);
            }
        }
        return articleTypeTreeDtos;
    }

    /**
     * 获取该分类下的文章
     */
    public List<ArticleTypeTreeDto> getChildArticle(Map<Long, List<Article>> articleGroup,String pkey,Long id){
        List<ArticleTypeTreeDto> articleTypeTreeDtos = null;
        List<Article> collect =articleGroup.getOrDefault(id,new ArrayList<>());
        for (Article item : collect) {
            if(articleTypeTreeDtos ==null){
                articleTypeTreeDtos =  new ArrayList<>();
            }
            ArticleTypeTreeDto articleTypeTreeDto = new ArticleTypeTreeDto();
            String key = pkey+"-A"+item.getId();
            articleTypeTreeDto.setKey(key);
            articleTypeTreeDto.setTitle(item.getTitle());
            articleTypeTreeDtos.add(articleTypeTreeDto);
        }
        return articleTypeTreeDtos;
    }


    /**
     * 递归组装数据
     */

    public List<ArticleTypeTreeDto> getChild(Long id,List<ArticleType> articleTypes,String pkey,Map<Long, List<Article>> articleGroup){
        List<ArticleTypeTreeDto> articleTypeTreeDtos = null;
        List<ArticleType> articleTypesFilter= articleTypes.stream().filter(item -> item.getPid().equals(id)).collect(Collectors.toList());
        for (ArticleType item : articleTypesFilter) {
            if(articleTypeTreeDtos == null){
                articleTypeTreeDtos = new ArrayList<>();
            }
            ArticleTypeTreeDto articleTypeTreeDto = new ArticleTypeTreeDto();
            String key = pkey+"-"+item.getId();
            articleTypeTreeDto.setKey(key);
            articleTypeTreeDto.setTitle(item.getName());
            List<ArticleTypeTreeDto> child = getChild(item.getId(), articleTypes, key,articleGroup);
            List<ArticleTypeTreeDto> childArticle = getChildArticle(articleGroup, key, item.getId());
            if(child == null){
                child = childArticle;
            }else{
                child.addAll(childArticle);
            }
            articleTypeTreeDto.setChildren(child);
            articleTypeTreeDtos.add(articleTypeTreeDto);
        }
        return articleTypeTreeDtos;
    }



}
