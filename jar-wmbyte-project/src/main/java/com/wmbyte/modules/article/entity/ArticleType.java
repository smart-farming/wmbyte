package com.wmbyte.modules.article.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author lisw
 * @program wmbyte
 * @description
 * @createDate 2023-04-02 01:28:33
 **/
@TableName("t_article_type")
@Data
public class ArticleType {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private Long pid;
}
