package com.wmbyte.modules.article.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author lisw
 * @program codeingbox
 * @description 文章
 * @createDate 2023-03-31 22:33:44
 **/
@Data
@TableName("t_article")
public class Article {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String title;

    private String content;

    private Long pid;

    private Integer sort;

    private Long lastModified;
}
