package com.wmbyte.modules.article.dto;

import lombok.Data;

import java.util.List;

/**
 * @author lisw
 * @program wmbyte
 * @description
 * @createDate 2023-04-02 11:40:21
 **/
@Data
public class ArticleTypeTreeDto {

    private String title;

    private String key;

    private List<ArticleTypeTreeDto> children;

}
