package com.wmbyte.modules.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wmbyte.modules.article.dto.ArticleTypeTreeDto;
import com.wmbyte.modules.article.entity.ArticleType;
import com.wmbyte.modules.article.mapper.ArticleTypeMapper;

import java.util.List;

/**
 * @author lisw
 * @program wmbyte
 * @description
 * @createDate 2023-04-02 11:32:10
 **/
public interface ArticleTypeService extends IService<ArticleType> {

    List<ArticleTypeTreeDto> listAll();

}
