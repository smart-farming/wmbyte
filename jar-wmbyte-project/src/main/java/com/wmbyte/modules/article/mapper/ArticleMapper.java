package com.wmbyte.modules.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wmbyte.modules.article.entity.Article;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author lisw
 * @program codeingbox
 * @description
 * @createDate 2023-03-31 22:35:13
 **/
public interface ArticleMapper extends BaseMapper<Article> {
}
