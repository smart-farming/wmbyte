package com.wmbyte.modules.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wmbyte.modules.article.entity.Article;

/**
 * @author lisw
 * @program codeingbox
 * @description
 * @createDate 2023-03-31 22:35:59
 **/
public interface ArticleService extends IService<Article> {


}
