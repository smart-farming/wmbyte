package com.wmbyte.modules.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wmbyte.modules.article.entity.ArticleType;

/**
 * @author lisw
 * @program wmbyte
 * @description
 * @createDate 2023-04-02 11:31:35
 **/
public interface ArticleTypeMapper extends BaseMapper<ArticleType> {
}
