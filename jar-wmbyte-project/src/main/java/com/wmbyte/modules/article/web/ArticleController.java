package com.wmbyte.modules.article.web;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wmbyte.modules.article.dto.ArticleTypeTreeDto;
import com.wmbyte.modules.article.entity.Article;
import com.wmbyte.modules.article.entity.ArticleType;
import com.wmbyte.modules.article.service.ArticleService;
import com.wmbyte.modules.article.service.ArticleTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author lisw
 * @program codeingbox
 * @description
 * @createDate 2023-03-31 22:41:15
 **/
@RestController
@RequestMapping("/article")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private ArticleTypeService articleTypeService;



    /**
     *
     * @return
     */
    @GetMapping("/getTypeList")
    public JSONObject getTypeList(){
        List<ArticleTypeTreeDto> list = articleTypeService.listAll();
        JSONObject rtnJson = new JSONObject();
        rtnJson.put("code",0);
        rtnJson.put("data",list);
        return rtnJson;
    }

    /**
     *
     * @param id
     * @return
     */
    @GetMapping("/getArticle")
    public JSONObject getList(Long id){
        JSONObject rtnJson = new JSONObject();
        rtnJson.put("code",0);
        Article article = articleService.getById(id);
        rtnJson.put("data",article);
        return rtnJson;
    }

    /**
     * 同步本地文件到数据库
     */
    @GetMapping("/fileWrite")
    public String test(String name){
        String dirPrefix = "/Users/lisw/work/my-project/helloworld-document/";

            File file = new File(dirPrefix);
            try {
                printFile(file,0L);
            } catch (Exception e) {
                e.printStackTrace();
            }
        return "1";
    }


    public  void printFile(File file,Long pid) throws Exception{
        String [] dirs = new String[]{"开发工具","环境搭建","秒杀系统设计","面试题"};
        if (file.isFile()) {
            System.out.println("您给定的是一个文件"); // 判断给定目录是否是一个合法的目录，如果不是，输出提示
        } else {
            File[] fileLists = file.listFiles(); // 如果是目录，获取该目录下的内容集合

            if(fileLists != null ) {
                for (int i = 0; i < fileLists.length; i++) { // 循环遍历这个集合内容
                    File item = fileLists[i];
                    String dirName = item.getName();
                    if (item.isDirectory() && Arrays.stream(dirs).anyMatch(t->t.equals(dirName))) {    //判断元素是不是一个目录
                        if(dirName.equals("images")){
                            continue;
                        }
                        //目录是否已经存在
                        QueryWrapper queryWrapper = new QueryWrapper();
                        queryWrapper.eq("name",item.getName());
                        ArticleType articleType = articleTypeService.getOne(queryWrapper,false);
                        if(articleType == null){
                            //创建目录
                            articleType = new ArticleType();
                            articleType.setPid(pid);
                            articleType.setName(item.getName());
                            articleTypeService.save(articleType);
                        }
                        printFile(item,articleType.getId());    //如果是目录，继续调用本方法来输出其子目录
                    } else {
                        //根目录下文件，忽略
                        if(pid==0L){
                            continue;
                        }
                        String line = null;
                        BufferedReader br = new BufferedReader(new FileReader(item));
                        StringBuffer content = new StringBuffer();
                        while ((line = br.readLine()) !=null){
                            content.append(line+"\r\n");
                        }
                        long l = item.lastModified();
                        QueryWrapper queryWrapper = new QueryWrapper();
                        queryWrapper.eq("title",item.getName());
                        Article one = articleService.getOne(queryWrapper,false);
                        if(one!=null){
                            if(one.getLastModified()==null || item.lastModified() != one.getLastModified()){
                                one.setLastModified(item.lastModified());
                                one.setContent(content.toString());
                                articleService.updateById(one);
                            }
                        }else{
                            Article article = new Article();
                            article.setTitle(item.getName());
                            article.setSort(i);
                            article.setContent(content.toString());
                            article.setPid(pid);
                            article.setLastModified(item.lastModified());
                            articleService.save(article);
                        }
                    }
                }
            }
        }
    }
}
