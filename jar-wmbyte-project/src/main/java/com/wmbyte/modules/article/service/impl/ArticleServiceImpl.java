package com.wmbyte.modules.article.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wmbyte.modules.article.entity.Article;
import com.wmbyte.modules.article.mapper.ArticleMapper;
import com.wmbyte.modules.article.service.ArticleService;
import org.springframework.stereotype.Service;

/**
 * @author lisw
 * @program codeingbox
 * @description
 * @createDate 2023-03-31 22:36:24
 **/
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper,Article> implements ArticleService {
}
